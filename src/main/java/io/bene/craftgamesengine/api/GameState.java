package io.bene.craftgamesengine.api;

/**
 * Created by Bene on 03.08.2015.
 */
public enum GameState {

    WAITING {
        @Override
        public String toString() {
            return "Waiting";
        }
    },
    LOBBY {
        @Override
        public String toString() {
            return "Lobby";
        }
    },
    WARMUP {
        @Override
        public String toString() {
            return "Warmup";
        }
    },
    STARTED {
        @Override
        public String toString() {
            return "Started";
        }
    },
    ENDING {
        @Override
        public String toString() {
            return "Ending";
        }
    },
    UNKNOW {
        @Override
        public String toString() {
            return "Unknown / Custom";
        }
    },
    ADMIN {
        @Override
        public String toString() {
            return "Admin";
        }
    };

}