package io.bene.craftgamesengine.api;

/**
 * Created by Bene on 03.08.2015.
 */
public enum GameRule {

    CLEAR_WEATHER(true),
    PVP(true),
    BREAK_BLOCKS(true),
    PLACE_BLOCKS(true),
    BLOCK_FIRE_DAMAGE(true),
    ENTITY_DAMAGE_BY_PLAYER(true),
    HUNGER(true),
    PLAYER_DAMAGE_BY_ENTITY(true);

    private boolean defaultValue;

    GameRule(boolean defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean getDefaultValue() {
        return defaultValue;
    }
}