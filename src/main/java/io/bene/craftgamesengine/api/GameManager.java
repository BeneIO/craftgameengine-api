package io.bene.craftgamesengine.api;

import io.bene.craftgamesengine.api.achievement.Achievement;
import io.bene.craftgamesengine.api.achievement.AchievementManager;
import io.bene.craftgamesengine.api.arena.Arena;
import io.bene.craftgamesengine.api.arena.ArenaLoader;
import io.bene.craftgamesengine.api.arena.ArenaManager;
import io.bene.craftgamesengine.api.data.DatabaseManager;
import io.bene.craftgamesengine.api.net.Server;
import io.bene.craftgamesengine.api.statistic.Statistic;
import io.bene.craftgamesengine.api.statistic.StatisticManager;
import io.bene.craftgamesengine.api.team.TeamManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

/**
 * Created by Bene on 03.08.2015.
 */
public interface GameManager {

    void setSpectator(Player player);
    boolean isPlayer(Player player);

    List<UUID> getPlayers();

    GameRegistry getGameRegistry();

    void initializeAchievementManager(Achievement... achievements);
    AchievementManager getAchievementManager();

    <T extends Arena> void initializeArenaManager(Class<T> clazz, ArenaLoader<T> arenaLoader);
    <T extends Arena> ArenaManager<T> getArenaManager();

    <T extends Statistic> void initializeStatisticManager(Class<T> clazz);
    <T extends Statistic> StatisticManager<T> getStatisticManager();

    void initializeTeamManager(int teamCount);
    TeamManager getTeamManager();

    DatabaseManager getDatabaseManager();

    Game getGame();

    GameState getGameState();
    void setGameState(GameState gameState);

    Server getServer();

    Location LOBBY_LOCATION = null;
}