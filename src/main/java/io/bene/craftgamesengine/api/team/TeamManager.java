package io.bene.craftgamesengine.api.team;

import org.bukkit.entity.Player;

/**
 * Created by Bene on 03.08.2015.
 */
public interface TeamManager {

    int getTeamCount();

    Team[] getTeams();
    Team getTeamById(int id);

    Team getTeamForPlayer(Player player);

}