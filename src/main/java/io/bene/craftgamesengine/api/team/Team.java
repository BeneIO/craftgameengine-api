package io.bene.craftgamesengine.api.team;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by Bene on 03.08.2015.
 */
public enum  Team {

    RED(1, "Red", ChatColor.RED, Color.RED, (byte) 14, (byte) 14),
    BLUE(2, "Blue", ChatColor.BLUE, Color.BLUE, (byte) 11, (byte) 3),
    GREEN(3, "Green", ChatColor.GREEN, Color.GREEN, (byte) 5, (byte) 5),
    YELLOW(4, "Yellow", ChatColor.YELLOW, Color.YELLOW, (byte) 4, (byte) 4);

    private int id;
    private String name;
    private ChatColor chatColor;
    private Color armorColor;

    private ItemStack wool;
    private ItemStack clay;

    Team(int id, String name, ChatColor chatColor, Color armorColor, byte woolDamage, byte clayDamage) {

        this.id = id;
        this.name = name;
        this.chatColor = chatColor;
        this.armorColor = armorColor;

        this.wool = new ItemStack(Material.WOOL, 1, woolDamage);
        this.clay = new ItemStack(Material.STAINED_CLAY, 1, clayDamage);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public Color getArmorColor() {
        return armorColor;
    }

    public ItemStack getWoolItem() {
        return wool.clone();
    }

    public ItemStack getClayItem() {
        return clay.clone();
    }

    public List<UUID> getPlayers() {
        // TODO
        return Collections.emptyList();
    }
}