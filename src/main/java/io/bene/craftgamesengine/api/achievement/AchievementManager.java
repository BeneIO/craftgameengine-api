package io.bene.craftgamesengine.api.achievement;

import org.bukkit.entity.Player;

/**
 * Created by Bene on 03.08.2015.
 */
public interface AchievementManager {

    void callAchievement(Player player, Achievement achievement);

}