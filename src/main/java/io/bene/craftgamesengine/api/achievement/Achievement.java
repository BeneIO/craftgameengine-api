package io.bene.craftgamesengine.api.achievement;

import java.io.Serializable;

/**
 * Created by Bene on 03.08.2015.
 */
public final class Achievement implements Serializable {

    private String id;
    private String icon;

    public Achievement() {}

    public Achievement(String id, String icon) {
        this.id = id;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public String getIcon() {
        return icon;
    }
}