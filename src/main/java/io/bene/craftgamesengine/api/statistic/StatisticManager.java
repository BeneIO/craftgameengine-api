package io.bene.craftgamesengine.api.statistic;

import java.util.UUID;
import java.util.function.Consumer;

/**
 * Created by Bene on 03.08.2015.
 */
public interface StatisticManager<T extends Statistic> {

    void getStatistic(UUID holder, Consumer<T> consumer);

}