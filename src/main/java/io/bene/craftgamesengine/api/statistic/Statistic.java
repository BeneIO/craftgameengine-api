package io.bene.craftgamesengine.api.statistic;

import java.io.Serializable;
import java.util.SortedMap;

/**
 * Created by Bene on 03.08.2015.
 */
public interface Statistic extends Serializable {

    SortedMap<String, Double> toFormatStrings();

}