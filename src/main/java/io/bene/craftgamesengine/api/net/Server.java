package io.bene.craftgamesengine.api.net;

/**
 * Created by Bene on 09.09.2015.
 */
public interface Server {

    String getId();
    ServerInformation getServerInformation();
}