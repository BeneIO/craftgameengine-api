package io.bene.craftgamesengine.api.net;

/**
 * Created by Bene on 09.09.2015.
 */
public interface ServerInformation {

    String getMetadata(String key);
    void setMetadata(String key, Object data);
    void update();
}