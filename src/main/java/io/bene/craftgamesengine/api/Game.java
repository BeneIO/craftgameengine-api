package io.bene.craftgamesengine.api;

import io.bene.craftgamesengine.CoreGameManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Random;

/**
 * Created by Bene on 03.08.2015.
 */
public abstract class Game extends JavaPlugin {

    private GameManager gameManager;
    private Random random;

    public Game() {
        gameManager = new CoreGameManager(this);
        random = new Random();
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    @Override
    public final void onEnable() {
        ((CoreGameManager) getGameManager()).preInit();
        init();
        ((CoreGameManager) getGameManager()).postInit();
    }

    @Override
    public final void onDisable() {
        ((CoreGameManager) getGameManager()).exit();
        exit();
    }

    public abstract void init();
    public abstract void exit();

    public final String getDisplayName() {
        return getDescription().getPrefix() == null ? getName() : getDescription().getPrefix();
    }

    public Random getRandom() {
        return random;
    }
}