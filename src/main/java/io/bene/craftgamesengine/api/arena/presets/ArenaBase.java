package io.bene.craftgamesengine.api.arena.presets;

import io.bene.craftgamesengine.api.arena.Arena;
import io.bene.craftgamesengine.api.arena.Spawn;
import org.bukkit.entity.Player;

/**
 * Created by Bene on 19.09.2015.
 */
public abstract class ArenaBase implements Arena {

    private String name;
    private String displayName;
    private String icon;
    private String metaData;
    private int minimalPlayers;
    private int maximalPlayers;
    private int radius;
    private Spawn middle;

    public ArenaBase(String name, String displayName, String icon, String metaData, int minimalPlayers, int maximalPlayers, int radius, Spawn middle) {
        this.name = name;
        this.displayName = displayName;
        this.icon = icon;
        this.metaData = metaData;
        this.minimalPlayers = minimalPlayers;
        this.maximalPlayers = maximalPlayers;
        this.radius = radius;
        this.middle = middle;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public String getMetaData() {
        return metaData;
    }

    @Override
    public int getMinimalPlayers() {
        return minimalPlayers;
    }

    @Override
    public int getMaximalPlayers() {
        return maximalPlayers;
    }

    @Override
    public int getRadius() {
        return radius;
    }

    @Override
    public Spawn getMiddle() {
        return middle;
    }

    @Override
    public abstract void spawnPlayers();

    @Override
    public abstract void spawnPlayer(Player player);
}