package io.bene.craftgamesengine.api.arena.presets;

import io.bene.craftgamesengine.api.arena.Spawn;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Bene on 08.09.2015.
 */
public class SoloArena extends ArenaBase {

    private List<Spawn> spawns;

    public SoloArena() {
        super("template", "Template", Material.BARRIER.name(), "", 2, 4, 15, new Spawn(0, 60, 0, 0F, 0F));
        spawns = new ArrayList<>();
        spawns.add(new Spawn(0, 60, 0, 0F, 0F));
    }

    public SoloArena(String name, String displayName, String icon, String metaData, int minimalPlayers, int maximalPlayers, int radius, Spawn middle, List<Spawn> spawns) {
        super(name, displayName, icon, metaData, minimalPlayers, maximalPlayers, radius, middle);
        this.spawns = spawns;
    }

    @Override
    public void spawnPlayers() {
        Iterator<Spawn> iterator = getSpawns().iterator();
        World world = Bukkit.getWorld(getName());
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!iterator.hasNext()) {
                iterator = getSpawns().iterator();
            }
            p.teleport(iterator.next().toLocation(world));
        }
    }

    @Override
    public void spawnPlayer(Player player) {
        /*

        TODO:

        Spawn[] spawns = getSpawns().toArray(new Spawn[getSpawns().size()]);
        double distance = 0;
        int bestSpawn = 0;
        for (Player p : CoreGameManager.getGameManager()) {
            for (int i = 0; i <= spawns.length; i++) {
                double distanceSquared = p.getLocation().distanceSquared(spawns[i].toLocation(p.getWorld()));
                if (distanceSquared > distance) {
                    distance = distanceSquared;
                    bestSpawn = i;
                }
            }
        }
        player.teleport(spawns[bestSpawn].toLocation())
        */
    }

    public List<Spawn> getSpawns() {
        return spawns;
    }
}