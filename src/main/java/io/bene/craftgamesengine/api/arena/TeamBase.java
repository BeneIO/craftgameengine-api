package io.bene.craftgamesengine.api.arena;

import io.bene.craftgamesengine.api.team.Team;

/**
 * Created by Bene on 03.08.2015.
 */
public class TeamBase {

    private transient Team tTeam;
    private String team;
    private Spawn spawn;

    private int minX, minY, minZ, maxX, maxY, maxZ;

    public TeamBase(String team, Spawn spawn, int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {

        this.tTeam = Team.valueOf(team.toUpperCase());
        this.team = team.toUpperCase();
        this.spawn = spawn;
        this.minX = minX;
        this.minY = minY;
        this.minZ = minZ;
        this.maxX = maxX;
        this.maxY = maxY;
        this.maxZ = maxZ;

    }

    public TeamBase(Team team, Spawn spawn, int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {

        this.tTeam = team;
        this.team = team.name();
        this.spawn = spawn;
        this.minX = minX;
        this.minY = minY;
        this.minZ = minZ;
        this.maxX = maxX;
        this.maxY = maxY;
        this.maxZ = maxZ;

    }

    public Team getTeam() {
        return tTeam;
    }

    public Spawn getSpawn() {
        return spawn;
    }

    public int getMinX() {
        return minX;
    }

    public int getMinY() {
        return minY;
    }

    public int getMaxZ() {
        return maxZ;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getMinZ() {
        return minZ;
    }
}