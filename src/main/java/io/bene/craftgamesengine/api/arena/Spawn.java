package io.bene.craftgamesengine.api.arena;

import org.bukkit.Location;
import org.bukkit.World;

/**
 * Created by Bene on 03.08.2015.
 */
public class Spawn {

    private int x, y, z;
    private float yaw, pitch;

    public Spawn(int x, int y, int z, float yaw, float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public float getYaw() {
        return yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public Location toLocation(World world) {
        return new Location(world, x, y, z, yaw, pitch);
    }
}