package io.bene.craftgamesengine.api.arena.presets;

import io.bene.craftgamesengine.CoreGameManager;
import io.bene.craftgamesengine.api.GameManager;
import io.bene.craftgamesengine.api.arena.Spawn;
import io.bene.craftgamesengine.api.arena.TeamBase;
import io.bene.craftgamesengine.api.team.Team;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bene on 08.09.2015.
 */
public class TeamArena extends ArenaBase {

    private Map<String, TeamBase> teamBases;
    private transient Map<Team, TeamBase> tTeamBases;
    private int teamCount;

    public TeamArena() {
        super("template", "Template", Material.BARRIER.name(), "", 2, 4, 15, new Spawn(0, 60, 0, 0F, 0F));
        teamBases = new HashMap<>();
        teamBases.put(Team.BLUE.name(), new TeamBase(Team.BLUE, new Spawn(3, 0, 3, 0F, 0F), 0, 0, 0, 5, 5, 5));
        teamBases.put(Team.RED.name(), new TeamBase(Team.RED, new Spawn(13, 0, 13, 0F, 0F), 10, 10, 10, 15, 15, 15));
        teamCount = 2;
        tTeamBases = new HashMap<>();
        teamBases.forEach((team, base) -> tTeamBases.put(Team.valueOf(team), base));
    }

    public TeamArena(String name, String displayName, String icon, String metaData, int minimalPlayers, int maximalPlayers, int radius, Spawn middle, int teamCount, Map<String, TeamBase> teamBases) {
        super(name, displayName, icon, metaData, minimalPlayers, maximalPlayers, radius, middle);
        this.teamCount = teamCount;
        this.teamBases = teamBases;
        tTeamBases = new HashMap<>();
        teamBases.forEach((team, base) -> tTeamBases.put(Team.valueOf(team), base));
    }


    @Override
    public void spawnPlayers() {
        tTeamBases.forEach((team, base) -> {
            team.getPlayers().forEach((id) -> {
                Player p = Bukkit.getPlayer(id);
                p.teleport(base.getSpawn().toLocation(Bukkit.getWorld(CoreGameManager.getGameManager().getArenaManager().getCurrentArena().getName())));
            });
        });
    }

    @Override
    public void spawnPlayer(Player player) {
        Team team = CoreGameManager.getGameManager().getTeamManager().getTeamForPlayer(player);
        if (team != null) {
            player.teleport(tTeamBases.get(team).getSpawn().toLocation(Bukkit.getWorld(CoreGameManager.getGameManager().getArenaManager().getCurrentArena().getName())));
        } else {
            player.teleport(GameManager.LOBBY_LOCATION);
        }
    }

    public Map<Team, TeamBase> getTeamBases() {
        return tTeamBases;
    }

    public int getTeamCount() {
        return teamCount;
    }
}