package io.bene.craftgamesengine.api.arena;

import java.util.List;

/**
 * Created by Bene on 03.08.2015.
 */
public interface ArenaManager<T extends Arena> {

    T getCurrentArena();
    T getArena(String name);

    List<T> getArenas();

    boolean setArena(T arena);

}