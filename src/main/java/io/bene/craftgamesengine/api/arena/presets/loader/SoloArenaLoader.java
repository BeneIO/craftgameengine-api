package io.bene.craftgamesengine.api.arena.presets.loader;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import io.bene.craftgamesengine.CoreGameManager;
import io.bene.craftgamesengine.api.arena.ArenaLoader;
import io.bene.craftgamesengine.api.arena.presets.SoloArena;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Bene on 19.09.2015.
 */
public class SoloArenaLoader implements ArenaLoader<SoloArena> {

    private SoloArena[] arenas;

    @Override
    public List<SoloArena> loadArenas() {

        JsonDocument document = CoreGameManager.getGameManager().getDatabaseManager().getBucket().get("arenas-solo");
        if (document == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(CoreGameManager.getGameManager().getDatabaseManager().getGson().fromJson(document.content().toString(), SoloArenaLoader.class).arenas);
    }

    @Override
    public void saveArenas(List<SoloArena> arenas) {
        this.arenas = arenas.toArray(new SoloArena[arenas.size()]);
        CoreGameManager.getGameManager().getDatabaseManager().getBucket().upsert(JsonDocument.create("arenas-solo", JsonObject.fromJson(CoreGameManager.getGameManager().getDatabaseManager().getGson().toJson(this))));
    }
}