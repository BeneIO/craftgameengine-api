package io.bene.craftgamesengine.api.arena;

import java.util.List;

/**
 * Created by Bene on 19.09.2015.
 */
public interface ArenaLoader<T extends Arena> {

    List<T> loadArenas();
    void saveArenas(List<T> arenas);

}