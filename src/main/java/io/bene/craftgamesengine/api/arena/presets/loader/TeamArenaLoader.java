package io.bene.craftgamesengine.api.arena.presets.loader;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import io.bene.craftgamesengine.CoreGameManager;
import io.bene.craftgamesengine.api.arena.ArenaLoader;
import io.bene.craftgamesengine.api.arena.presets.TeamArena;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Bene on 19.09.2015.
 */
public class TeamArenaLoader<T extends TeamArena> implements ArenaLoader<T> {

    private T[] arenas;
    private transient Class<T> clazz;

    public TeamArenaLoader(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public List<T> loadArenas() {

        JsonDocument document = CoreGameManager.getGameManager().getDatabaseManager().getBucket().get("arenas");
        if (document == null) {
            return Collections.emptyList();
        }
        return CoreGameManager.getGameManager().getDatabaseManager().getGson().fromJson(document.content().toString(), TeamArenaLoader<T>.class);
    }

    @Override
    public void saveArenas(List<T> arenas) {
        this.arenas = (T[]) Array.newInstance(clazz, arenas.size());
        for (int i = 0; i < arenas.size(); i++) {
            this.arenas[i] = arenas.get(i);
        }
        CoreGameManager.getGameManager().getDatabaseManager().getBucket().upsert(JsonDocument.create("arenas", JsonObject.fromJson(CoreGameManager.getGameManager().getDatabaseManager().getGson().toJson(this))));
    }
}