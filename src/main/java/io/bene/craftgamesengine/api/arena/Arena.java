package io.bene.craftgamesengine.api.arena;

import org.bukkit.entity.Player;

/**
 * Created by Bene on 03.08.2015.
 */
public interface Arena {

    String getName();
    String getDisplayName();
    String getIcon();
    String getMetaData();

    int getMinimalPlayers();
    int getMaximalPlayers();
    int getRadius();

    Spawn getMiddle();

    void spawnPlayers();
    void spawnPlayer(Player player);

}