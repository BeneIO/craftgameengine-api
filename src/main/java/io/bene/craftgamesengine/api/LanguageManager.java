package io.bene.craftgamesengine.api;

import io.bene.craftgamesengine.CoreGameManager;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Bene on 09.09.2015.
 */
public class LanguageManager {

    private static ResourceBundle gameDe = ResourceBundle.getBundle(CoreGameManager.getGameManager().getGame().getName(), Locale.GERMAN);
    private static ResourceBundle gameEn = ResourceBundle.getBundle(CoreGameManager.getGameManager().getGame().getName(), Locale.ENGLISH);

    private static ResourceBundle coreDe = ResourceBundle.getBundle("craftgameengine", Locale.GERMAN);
    private static ResourceBundle coreEn = ResourceBundle.getBundle("craftgameengine", Locale.ENGLISH);

    public static void sendMessage(Player player, String mseKey, Object... with) {
        sendMessage(player, (byte)  0, mseKey, with);
    }

    public static void sendMessage(Player player, byte type, String mseKey, Object... with) {

        if (!coreDe.containsKey(mseKey) && !gameDe.containsKey(mseKey)) {
            return;
        }

        if (type == 0) {

            String msg = null;
            if (gameDe.containsKey(mseKey)) {
                msg = gameDe.getString(mseKey);
            } else {
                msg = coreDe.getString(mseKey);
            }

            player.spigot().sendMessage(ComponentSerializer.parse(MessageFormat.format(msg, with)));
        } else {

            String msg = null;
            if (gameDe.containsKey(mseKey)) {
                msg = gameDe.getString(mseKey);
            } else {
                msg = coreDe.getString(mseKey);
            }

            PacketPlayOutChat packet = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(MessageFormat.format(msg, with)), type);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
        }
    }

    public static void sendTitle(Player player, String titleKey, String subTitleKey, int fadeIn, int stay, int fadeOut, Object... with) {

        if ((!coreDe.containsKey(titleKey) && !gameDe.containsKey(titleKey)) || (!coreDe.containsKey(subTitleKey) && !gameDe.containsKey(subTitleKey))) {
            return;
        }

        String title = null;
        if (gameDe.containsKey(titleKey)) {
            title = gameDe.getString(titleKey);
        } else {
            title = coreDe.getString(titleKey);
        }

        String subTitle = null;
        if (gameDe.containsKey(subTitleKey)) {
            subTitle = gameDe.getString(subTitleKey);
        } else {
            subTitle = coreDe.getString(subTitleKey);
        }

        PacketPlayOutTitle timings = new PacketPlayOutTitle(fadeIn, stay, fadeOut);
        PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a(MessageFormat.format(title, with)));
        PacketPlayOutTitle subTitlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a(MessageFormat.format(subTitle, with)));

        PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;
        playerConnection.sendPacket(timings);
        playerConnection.sendPacket(titlePacket);
        playerConnection.sendPacket(subTitlePacket);

    }

    public static String get(Locale locale, String mseKey, Object... with) {

        if (!coreDe.containsKey(mseKey) && !gameDe.containsKey(mseKey)) {
            return null;
        }

        String msg = null;
        if (gameDe.containsKey(mseKey)) {
            msg = gameDe.getString(mseKey);
        } else {
            msg = coreDe.getString(mseKey);
        }

        return TextComponent.toLegacyText(ComponentSerializer.parse(MessageFormat.format(msg, with)));
    }
}