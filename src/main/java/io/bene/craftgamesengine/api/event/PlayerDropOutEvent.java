package io.bene.craftgamesengine.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Created by Bene on 18.09.2015.
 */
public class PlayerDropOutEvent extends PlayerEvent {

    public PlayerDropOutEvent(Player who) {
        super(who);
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}