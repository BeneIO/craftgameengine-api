package io.bene.craftgamesengine.api.event;

import io.bene.craftgamesengine.api.Game;
import io.bene.craftgamesengine.api.GameState;
import org.bukkit.event.HandlerList;

/**
 * Created by Bene on 18.09.2015.
 */
public class GameTimeChangeEvent extends GameEvent {

    private int time;

    public GameTimeChangeEvent(Game game, int time) {
        super(game);
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    public GameState getGameState() {
        return getGame().getGameManager().getGameState();
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}