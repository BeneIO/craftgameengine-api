package io.bene.craftgamesengine.api.event;

import io.bene.craftgamesengine.api.Game;
import org.bukkit.event.Event;

/**
 * Created by Bene on 03.08.2015.
 */
public abstract class GameEvent extends Event {

    private final Game game;

    public GameEvent(Game game) {
        this.game = game;
    }

    public final Game getGame() {
        return game;
    }
}