package io.bene.craftgamesengine.api.event;

import io.bene.craftgamesengine.api.Game;
import io.bene.craftgamesengine.api.GameState;
import org.bukkit.event.HandlerList;

/**
 * Created by Bene on 03.08.2015.
 */
public class GameStateChangeEvent extends GameEvent {

    private GameState oldGameState;
    private GameState newGameState;

    public GameStateChangeEvent(Game game, GameState oldGameState, GameState newGameState) {
        super(game);
        this.oldGameState = oldGameState;
        this.newGameState = newGameState;
    }

    public GameState getOldGameState() {
        return oldGameState;
    }

    public GameState getNewGameState() {
        return newGameState;
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}