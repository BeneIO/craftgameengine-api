package io.bene.craftgamesengine.api;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bene on 03.08.2015.
 */
public class GameRegistry {

    private Map<GameRule, Boolean> gameRules = new HashMap<>();

    public void setGameRule(GameRule gameRule, boolean value) {
        gameRules.put(gameRule, value);
    }

    public Boolean getGameRule(GameRule gameRule) {
        return gameRules.containsKey(gameRule) ? gameRules.get(gameRule) : gameRule.getDefaultValue();
    }

    public Map<GameRule, Boolean> getGameRules() {
        return gameRules;
    }

}