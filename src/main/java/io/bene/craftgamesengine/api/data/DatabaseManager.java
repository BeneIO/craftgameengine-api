package io.bene.craftgamesengine.api.data;

import com.couchbase.client.java.Bucket;
import com.google.gson.Gson;

import java.util.concurrent.ExecutorService;

/**
 * Created by Bene on 08.09.2015.
 */
public interface DatabaseManager {

    Bucket getBucket();
    ExecutorService getExecutorService();
    Gson getGson();
}